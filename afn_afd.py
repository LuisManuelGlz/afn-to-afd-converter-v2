# (a|b)*abb

# variables globales
automata_final = []
estados_master = []
resultados_de_mover = []
estado = 0
estado_que_estamos_revisando = 0 # A, B, C...

# función que revisa si la expresión introducida no tiene paréntesis perdidos
# su parámetro es la expresión regular introducida por el usuario
# retorna True o False
def comprobador(expresion):
    contador = 0

    for c in expresion:
        if c == '(':
            contador += 1
        elif c == ')':
            contador -= 1
    if contador == 0:
        return True
    else:
        return False

# función que convierte de expresión regular a afn
def operaciones(expresion=None, operacion=None):
    global automata_final
    global estado
    contador = 0
    stack = []
    temp = []
    secciones = []
    sub_expresion = ''
    sub_expresion_2 = ''

    # bucle infinito hasta que el usuario introduzca algo
    while True and not expresion:
        expresion = input('Introduce tu expresión> ')
        # cuando haya na expresión, break
        if expresion:
            break

    # comprobamos que la expresión tenga bien sus paréntesis
    if comprobador(expresion):
        # si no hay paréntesis
        if '(' not in expresion:
            inicial = estado
            # si operación es |
            if '|' in expresion:
                secciones = expresion.split('|')
                for i, seccion in enumerate(secciones):
                    # en la unión siempre tenemos que empezar con la cadena vacía
                    automata_final.append({
                        'go_to': None,
                        'estado_inicial': [inicial],
                        'simbolo': 'vacio',
                        'estado_final': estado+1,
                        'return_to': None
                    })

                    estado += 1

                    # metemos todo lo que está dentro de la rama
                    for j, simbolo in enumerate(seccion):
                        automata_final.append({
                            'go_to': None,
                            'estado_inicial': [estado],
                            'simbolo': simbolo,
                            'estado_final': estado+1,
                            'return_to': None
                        })
                        if j == len(seccion)-1:
                            temp.append(estado+1)
                        estado += 1
                
                # si operación es *
                if operacion == '*':
                    automata_final.append({
                        'go_to': None,
                        'estado_inicial': temp,
                        'simbolo': 'vacio',
                        'estado_final': estado+1,
                        'return_to': inicial
                    })
                # si operación es +
                else:
                    automata_final.append({
                        'go_to': None,
                        'estado_inicial': temp,
                        'simbolo': 'vacio',
                        'estado_final': estado+1,
                        'return_to': None
                    })

                estado += 1
            # si solo es concatenación o mover
            else:
                for simbolo in expresion:
                    automata_final.append({
                        'go_to': None,
                        'estado_inicial': [estado],
                        'simbolo': simbolo,
                        'estado_final': estado+1,
                        'return_to': None
                    })
                    estado += 1
                if operacion == '*':
                    automata_final[estado-1].update(return_to = inicial)
        # si tiene paréntesis
        else:
            # busca cuántos paréntesis existen
            for p in expresion:
                if p == '(':
                    contador += 1

            if contador == 1:
                contador = 0
                    
            # si espresión es parecida a (ab)+ ó (ab)*
            if expresion[0] == '(' and (expresion[-1] == '+' or expresion[-1] == '*'):
                operacion = expresion[-1]
                inicial = estado
                
                automata_final.append({
                    'go_to': None,
                    'estado_inicial': [inicial],
                    'simbolo': 'vacio',
                    'estado_final': estado+1,
                    'return_to': None
                })
                estado += 1

                sub_expresion = expresion[1:-2]
                operaciones(sub_expresion, operacion)

                automata_final.append({
                    'go_to': None,
                    'estado_inicial': [estado],
                    'simbolo': 'vacio',
                    'estado_final': estado+1,
                    'return_to': None
                })
                
                automata_final[inicial].update(go_to = estado + 1)
                estado += 1
            # si expresión es parecida a ab(ba)*
            elif expresion.index('(') > 0:
                sub_expresion = expresion[0:expresion.index('(')] # >ab< (ba)*
                sub_expresion_2 = expresion[expresion.index('('):] # ab >(ba)*<

                # if ab| <-- no se debe de escapar el pipe!
                #      ^
                if sub_expresion[-1] == '|':
                    # limpiamos la sub_expresión (le quitamos el '|')
                    sub_expresion = expresion[0:expresion.index('(')-1]
                    
                    inicial = estado

                    # en la unión siempre tenemos que empezar con la cadena vacía
                    automata_final.append({
                        'go_to': None,
                        'estado_inicial': [inicial],
                        'simbolo': 'vacio',
                        'estado_final': estado+1,
                        'return_to': None
                    })

                    estado += 1

                    operaciones(sub_expresion)

                    temp.append(estado)

                    # segunda parte de la unión
                    automata_final.append({
                        'go_to': None,
                        'estado_inicial': [inicial],
                        'simbolo': 'vacio',
                        'estado_final': estado+1,
                        'return_to': None
                    })

                    estado += 1

                    operaciones(sub_expresion_2)

                    temp.append(estado)

                    # parte final de la unión
                    automata_final.append({
                        'go_to': None,
                        'estado_inicial': temp,
                        'simbolo': 'vacio',
                        'estado_final': estado+1,
                        'return_to': None
                    })

                    estado += 1
                else:
                    operaciones(sub_expresion)                
                    operaciones(sub_expresion_2)
            # si expresión es parecida a (ba)*ab
            elif expresion[0] and not (expresion[-1] == '+' or expresion[-1] == '*'):
                operacion_index = ''
                if '*' in expresion:
                    operacion_index = expresion.find('*')
                    operacion = expresion[operacion_index]
                else:
                    operacion_index = expresion.find('+')
                    operacion = expresion[operacion_index]

                sub_expresion = expresion[expresion.index('('):operacion_index+1]
                sub_expresion_2 = expresion[operacion_index+1:]

                # if (ba)*| <-- no se debe de escapar el pipe!
                #         ^
                if expresion[expresion.index(')')+2] == '|':
                    # limpiamos la sub_expresión_2 (le quitamos el '|')
                    sub_expresion_2 = expresion[operacion_index+2:]
                    
                    inicial = estado

                    # en la unión siempre tenemos que empezar con la cadena vacía
                    automata_final.append({
                        'go_to': None,
                        'estado_inicial': [inicial],
                        'simbolo': 'vacio',
                        'estado_final': estado+1,
                        'return_to': None
                    })

                    estado += 1

                    operaciones(sub_expresion)

                    temp.append(estado)

                    # segunda parte de la unión
                    automata_final.append({
                        'go_to': None,
                        'estado_inicial': [inicial],
                        'simbolo': 'vacio',
                        'estado_final': estado+1,
                        'return_to': None
                    })

                    estado += 1

                    operaciones(sub_expresion_2)

                    temp.append(estado)

                    # parte final de la unión
                    automata_final.append({
                        'go_to': None,
                        'estado_inicial': temp,
                        'simbolo': 'vacio',
                        'estado_final': estado+1,
                        'return_to': None
                    })

                    estado += 1
                else:
                    operaciones(sub_expresion, operacion)
                    operaciones(sub_expresion_2)

            # por si hay paréntesis dentro paréntesis
            # al menos moriremos intentando ser dioses
            else:
                print(':\'c')
                contador = 0

            # for i, c in enumerate(expresion):
            #     if c == '(':
            #         stack.append(i)
            #     elif c == ')':
            #         secciones.insert(contador, [stack.pop(), i, expresion[i+1]])
            #         contador += 1
        
        # # si el inicio está al final
        # if secciones[-1][0] == 0:
        #     sub_expresion = expresion[secciones[-1][0]+1:secciones[-1][1]]
        #     operacion = secciones[-1][2]

        #     # operaciones(sub_expresion, operacion)
        #     print(sub_expresion, operacion)
    else:
        print('Error: Los parétesis no coinciden')

    # print(expresion)
    # print(sub_expresion)
    # print(secciones)

def e_cerradura_s(automata):
    global estado
    global estados_master

    # estado_en_operacion = [1, None] # en qué estado estamos actualmente revisando
    estados = [0]

    # si existe un autómata que vaya directo por Kleene
    if automata[0]['go_to']:
        estados.append(automata[0]['go_to'])

    for i in range(estado):
        # si hay una cadena vacía
        if automata[i]['simbolo'] == 'vacio':
            # revisar si pertenece a los estados revisados
            if automata[i]['estado_inicial'][0] in estados:
                # TODO: agregar el go_to y el return_to del actual
                estados.append(automata[i]['estado_final'])
    print('A')
    print(estados)
    estados_master.append(estados)

def e_cerradura_t(automata, resultados):
    global estado
    estados = resultados
    revisados = []
    pasada = 1
    pasadas = 1

    # for i in range(len(estados)):
    #     print(estados[i])

    while pasada <= pasadas:
        for i in range(estado):
            if automata[i]['simbolo'] == 'E':
                if automata[i]['sub_estado_1'][0] in estados:
                    estados.append(automata[i]['sub_estado_2'])
                    if automata[i]['go_to'] != None and automata[i]['go_to'] not in revisados:
                        estados.append(automata[i]['go_to'])
                        revisados.append(automata[i]['go_to'])
                        pasadas += 1
                    if automata[i]['return_to'] != None and automata[i]['go_to'] not in revisados:
                        estados.append(automata[i]['return_to'])
                        revisados.append(automata[i]['go_to'])
                        pasadas += 1
                if automata[i]['sub_estado_1'][1] in estados:
                    if automata[i]['sub_estado_2'] not in estados:
                        estados.append(automata[i]['sub_estado_2'])
        pasada += 1

    print(estados)
    return { 'b': estados }

def mover(automata, T, simbolo):
    global estado
    resultados = []
    
    for i in range(estado):
        if automata[i]['simbolo'] == simbolo:
            if automata[i]['sub_estado_1'][0] in T:
                resultados.append(automata[i]['sub_estado_2'])

    print(resultados)
    return resultados

def e_cerradura_t2(automata_final, resultados):
    global estado
    global estados_master
    global estado_que_estamos_revisando
    estados = resultados
    revisados = []
    
    for i in range(estado):
        for j in range(len(automata_final[i]['estado_inicial'])):
            if automata_final[i]['estado_inicial'][j] in estados and automata_final[i]['estado_inicial'][j] not in revisados:
                if automata_final[i]['simbolo'] == 'vacio':
                    estados.append(automata_final[i]['estado_final'])
                    for k in range(len(automata_final[i]['estado_inicial'])):
                        revisados.append(automata_final[i]['estado_inicial'][k])
                
                if automata_final[i]['go_to'] != None or automata_final[i]['return_to'] != None:
                    # checamos que go_to tenga algo y que no haya sido revisado
                    if automata_final[i]['go_to'] != None and automata_final[i]['go_to'] not in revisados:
                        estados.append(automata_final[i]['go_to'])
                        revisados.append(automata_final[i]['go_to'])

                    # checamos que return_to tenga algo y que no haya sido revisado
                    if automata_final[i]['return_to'] != None and automata_final[i]['return_to'] not in revisados:
                        estados.append(automata_final[i]['return_to'])
                        revisados.append(automata_final[i]['return_to'])

                    # revisamos de nuevo porque se incluyó un nuevo estado y ya habiamos revisado
                    for l in range(estado):
                        for m in range(len(automata_final[l]['estado_inicial'])):
                            if automata_final[l]['estado_inicial'][m] in estados:
                                if automata_final[l]['simbolo'] == 'vacio':
                                    estados.append(automata_final[l]['estado_final'])
                                    for n in range(len(automata_final[l]['estado_inicial'])):
                                        revisados.append(automata_final[l]['estado_inicial'][n])

    estados_master.append(list(set(estados)))
    print(estados_master)

def mover2(automata_final):
    global estado
    global estados_master
    global estado_que_estamos_revisando # A, B, C...
    revisados = []
    resultados = []

    while True:
        if estados_master[-1] == [] and estados_master[-2] == []:
            break

        for i in range(estado):
            # por si el estado inicial es [0,1,2]
            for j in range(len(automata_final[i]['estado_inicial'])):
                if automata_final[i]['estado_inicial'][j] in estados_master[estado_que_estamos_revisando] and automata_final[i]['estado_inicial'][j] not in revisados:            
                    if automata_final[i]['simbolo'] == 'a':
                        resultados.append(automata_final[i]['estado_final'])
                        for k in range(len(automata_final[i]['estado_inicial'])):
                            revisados.append(automata_final[i]['estado_inicial'][k])

        e_cerradura_t2(automata_final, resultados)
        resultados = [] # vaciamos la lista porque la ocupa aquí abajo

        for i in range(estado):
            # por si el estado inicial es [0,1,2]
            for j in range(len(automata_final[i]['estado_inicial'])):
                if automata_final[i]['estado_inicial'][j] in estados_master[estado_que_estamos_revisando] and automata_final[i]['estado_inicial'][j] not in revisados:            
                    if automata_final[i]['simbolo'] == 'b':
                        resultados.append(automata_final[i]['estado_final'])
                        for k in range(len(automata_final[i]['estado_inicial'])):
                            revisados.append(automata_final[i]['estado_inicial'][k])

        e_cerradura_t2(automata_final, resultados)
        resultados = [] # vaciamos la lista porque la ocupa aquí abajo
        estado_que_estamos_revisando += 1

    print(resultados)
operaciones()

########## test starts here ###########
# estado = 5
# automata_final = [
#     {'go_to': None, 'estado_inicial': [0], 'simbolo': 'vacio', 'estado_final': 1, 'return_to': None},
#     {'go_to': None, 'estado_inicial': [1], 'simbolo': 'a', 'estado_final': 2, 'return_to': None},
#     {'go_to': None, 'estado_inicial': [0], 'simbolo': 'vacio', 'estado_final': 3, 'return_to': None},
#     {'go_to': None, 'estado_inicial': [3], 'simbolo': 'b', 'estado_final': 4, 'return_to': None},
#     {'go_to': None, 'estado_inicial': [2, 4], 'simbolo': 'vacio', 'estado_final': 5, 'return_to': None},
# ]

# estado = 10
# automata_final = [
#     {'go_to': 7, 'estado_inicial': [0], 'simbolo': 'vacio', 'estado_final': 1, 'return_to': None},
#     {'go_to': None, 'estado_inicial': [1], 'simbolo': 'vacio', 'estado_final': 2, 'return_to': None},
#     {'go_to': None, 'estado_inicial': [2], 'simbolo': 'a', 'estado_final': 3, 'return_to': None},
#     {'go_to': None, 'estado_inicial': [1], 'simbolo': 'vacio', 'estado_final': 4, 'return_to': None},
#     {'go_to': None, 'estado_inicial': [4], 'simbolo': 'b', 'estado_final': 5, 'return_to': None},
#     {'go_to': None, 'estado_inicial': [3, 5], 'simbolo': 'vacio', 'estado_final': 6, 'return_to': 1},
#     {'go_to': None, 'estado_inicial': [6], 'simbolo': 'vacio', 'estado_final': 7, 'return_to': None},
#     {'go_to': None, 'estado_inicial': [7], 'simbolo': 'a', 'estado_final': 8, 'return_to': None},
#     {'go_to': None, 'estado_inicial': [8], 'simbolo': 'b', 'estado_final': 9, 'return_to': None},
#     {'go_to': None, 'estado_inicial': [9], 'simbolo': 'b', 'estado_final': 10, 'return_to': None}
# ]
########## test ends here ###########

# vemos qué hay en el autómata
for automata in automata_final:
    print('{} ({})-{}-({}) {}'.format(
        '({})'.format(automata['go_to']) if automata['go_to'] else '   ',
        automata['estado_inicial'],
        automata['simbolo'],
        automata['estado_final'],
        '({})'.format(automata['return_to']) if automata['return_to'] else ''
    ))
print('Estado de aceptación', estado)

e_cerradura_s(automata_final)
mover2(automata_final)

# print('    Tabla de transición\n')
# print('          -----------------')
# print('          |    Símbolos   |')
# print('---------------------------')
# print('| Estados |   a   |   b   |')
# print('---------------------------')
